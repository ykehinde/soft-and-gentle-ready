module.exports = grunt => {
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-env');

    const mozjpeg = require('imagemin-mozjpeg');
    const imageminOptipng = require('imagemin-optipng');

    grunt.initConfig({
        uglify: {
            options: {
                banner: '/*! <%= grunt.template.today("dd-mm-yyyy h:m:s") %> */\n',
                sourceMap: true
            },
            application: {
                files: {
                    'dist/js/app.min.js': [
                        'dist/js/app.js'
                    ]
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/img/'
                }],
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg(), imageminOptipng()]
                },
            }
        },
        env: {
            prod: {
                NODE_ENV: 'production'
            }
        },
        browserify: {
            dist: {
                options: {
                    transform: [
                        [
                            'babelify',
                            {
                                presets: [
                                    'es2015',
                                    'react'
                                ]
                            }
                        ]
                    ]
                },
                src: ['src/js/components/*.js','src/index.js','src/Root.js'],
                dest: 'dist/js/app.js',
            }
        },
        sass: {
            dist: {
                options: {
                    trace: true,
                    style: 'compact'
                },
                files: {
                    'dist/css/style.css': 'src/scss/styles.scss'
                }
            }
        },
        postcss: {
            options: {
              // map: true, // inline sourcemaps

              // or
              map: {
                  inline: false, // save all sourcemaps as separate files...
                  annotation: 'dist/css/maps/' // ...to the specified directory
              },

              processors: [
                require('pixrem')(), // add fallbacks for rem units
                require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
                require('cssnano')() // minify the result
              ]
            },
            dist: {
              src: 'dist/css/*.css'
            }
          },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/css/style.min.css': ['dist/css/style.css']
                }
            }
        },
        copy: {
          main: {
            files: [
              {expand: true, cwd: 'src/fonts/', src: ['*'], dest: 'dist/fonts/'},
            ],
          },
        },
        watch: {
            browserify: {
                files: ['src/components/**/*.js', 'src/helpers/**/*.js', 'src/Root.js'],
                tasks: ['browserify', 'uglify']
            },
            scss: {
                files:['src/scss/**/*.scss'],
                tasks: ['sass', 'cssmin', 'postcss'],
                options: {
                    spawn: false,
                }
            },
            images: {
                files:['src/img/*.{png,jpg,gif}'],
                tasks: ['imagemin'],
                options: {
                    spawn: false,
                }
            },
            copy: {
                files:['src/fonts/**/*'],
                tasks: ['copy'],
                options: {
                    spawn: false,
                }
            }
        },
    });

    grunt.registerTask('default', [
        'browserify',
        'uglify',
        'imagemin',
        'cssmin',
        'sass',
        'postcss',
        'copy'
    ]);

    grunt.registerTask('prod', [
        'env',
        'browserify',
        'uglify',
        'imagemin',
        'cssmin',
        'sass',
        'postcss',
        'copy'
    ]);
};
