import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class RangeItem extends React.Component {
    constructor(props) {
      super(props)
      this.state = { isModalOpen: false }
    }
    render() {
        return (
            <div className="range__item">
                    <img src={this.props.imgSrc} title={this.props.videoHeadline} alt={this.props.videoHeadline} onClick={() => this.openModal()} />
                    <img className="play_btn" src="/dist/img/play_btn-purple.png" onClick={() => this.openModal()} />
                <Modal isOpen={this.state.isModalOpen} onClose={() => this.closeModal()}>
                    <button className="btn--close fa fa-close close" onClick={() => this.closeModal()}></button>
                    <ReactCSSTransitionGroup
                    className="fluidVideo"
                    component="div"
                    transitionName="fadeInUp"
                    transitionEnterTimeout={800}
                    transitionLeaveTimeout={800}
                    transitionAppear
                    transitionAppearTimeout={800}
                    >
                      <iframe className="fluidVideo-item" width="560" height="315" src={"https://www.youtube.com/embed/" +this.props.videoUrl + "?rel=0&amp;controls=0&amp;showinfo=0"} frameBorder="0" allowFullScreen></iframe>
                  </ReactCSSTransitionGroup>
                </Modal>
            </div>
        );
    }
    openModal() {
      this.setState({ isModalOpen: true });
      ga('send', 'event', this.props.videoHeadline, 'Click', `https://www.youtube.com/embed/${this.props.videoUrl}`);
      ga('10000heads.send', 'event', this.props.videoHeadline, 'Click', `https://www.youtube.com/embed/${this.props.videoUrl}`);
    }

    closeModal() {
      this.setState({ isModalOpen: false })
    }
}

class Modal extends React.Component {
  render() {
    if (this.props.isOpen === false)
      return null

    if (this.props.style) {
      for (let key in this.props.style) {
        modalStyle[key] = this.props.style[key]
      }
    }


    if (this.props.backdropStyle) {
      for (let key in this.props.backdropStyle) {
        backdropStyle[key] = this.props.backdropStyle[key]
      }
    }

    return (
      <div>
        <ReactCSSTransitionGroup
            className="modal"
            component="div"
            transitionName="fadeInUp"
            transitionEnterTimeout={800}
            transitionLeaveTimeout={800}
            transitionAppear
            transitionAppearTimeout={800}
        >
            {this.props.children}
        </ReactCSSTransitionGroup>

        {!this.props.noBackdrop &&
            <div className="backdrop"
                 onClick={e => this.close(e)}/>}
      </div>
    )
  }

  close(e) {
    e.preventDefault()

    if (this.props.onClose) {
      this.props.onClose()
    }
  }
}

export default RangeItem;
