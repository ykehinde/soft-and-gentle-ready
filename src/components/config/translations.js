export default {
    ScienceAnimation: {
        title: "Ready when you need us most",
        subtitle: "Subheading",
        paragraph: "We appreciate how hectic real life can be. Find out how the active ingredients in our Classic anti-perspirant range are ready to respond to your everyday challenges.",
        buttonText: "Learn more",
        href: "https://www.softandgentle.com/products/bodycare/classic/"
    },
    Quiz: {
        title: "Play the quiz",
        subtitle: "Snappy tagline",
        paragraph: "Herding kids or binge-watching Netflix? Play our quiz and we'll reveal your unique scene - real life chaos included!",
        facebook: "Login via Facebook",
        share: "Share",
        skipButton: "SKIP"
    },
    ProductOffer: {
        title: "Morrisons classic range 250ml now £1.50*",
        tandc: "*subject to stock availability, promotion starts 06/09/17 and ends 28/11/17",
        href:"https://groceries.morrisons.com/webshop/getSearchProducts.do?clearTabs=yes&isFreshSearch=true&chosenSuggestionPosition=0&entry=soft+and+gentle&dnr=y",
        trackingStore: "Morrisons",
        subtitle: "secondary tagline",
        buttonText: "Buy Now"
    },
    CampaignVideoRange: {
        title: "Ready for your daily challenges",
        paragraph: "What does real life throw at you? Soft & Gentle supports you through your everyday challenges around the clock.",
        video: {
            one: {
                videoImgSrc: "./dist/img/Softandgentle_VideoTumbnails_Yoga.png",
                videoHeadline: "Ready for 'quiet time'",
                videoUrl: "7RWsE6tbH-4"
            },
            two: {
                videoImgSrc: "./dist/img/Softandgentle_VideoTumbnails_DIY.png",
                videoHeadline: "Ready for DIY SOS",
                videoUrl: "0aAyKzJ7bO8"
            },
            three: {
                videoImgSrc: "./dist/img/Softandgentle_VideoTumbnails_Kids.png",
                videoHeadline: "Ready for energetic kids",
                videoUrl: "Ps2jfnbMIhw"
            },
        }
    },
    ProductRange: {
        productOne: {
            productName: "Classic range",
            productDesc: "Our Anti-Perspirant deodorants offer dependable protection that is kind to your skin, leaving you confident and protected all day long.",
            productSrc: "./dist/img/classic.png",
            productUrl: "https://www.softandgentle.com/products/bodycare/classic/"
        },
        productTwo: {
            productName: "0% Aluminium range",
            productDesc: "Our 0% Aluminium range offers three beautiful scents formulated with 0% aluminium and 0% alcohol, suitable for sensitive skin and vegan-friendly.",
            productSrc: "./dist/img/0_aluminium.png",
            productUrl: "https://www.softandgentle.com/products/bodycare/aluminium-free/"
        }
    }
} ;

