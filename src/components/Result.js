import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import parameters from '../../server/config/parameters';
import axios from 'axios';

const Result = ({options}) => {
  let gifId = options.id;
  let gifImage = (options.id) ? `${parameters.S3_BUCKET_BASE_URL}/${parameters.S3_BUCKET_NAME}/${gifId}.gif` : './dist/img/gif_generator-loading.gif';
  let shareLink, twitterLink;
  let downloadLink = `${parameters.S3_BUCKET_BASE_URL}/${parameters.S3_BUCKET_NAME}/${gifId}.gif`;

  if (gifId) {
    axios.post('https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAu6moOHhO0jEaedEPvsAST3u9LkETysrw',
      { longUrl: `${parameters.BASE_URL}/server/share/${gifId}`})
        .then(res => {
          shareLink = res.data.id;
          twitterLink = createTwitterLink();
        });
  }

  function renderResultOptions(key) {
    return (
      <span key={key}>{key}</span>
    );
  }

  function shareViaFacebook() {
      ga('send', 'event', 'Gif generator', 'share', 'Facebook');
      ga('1000heads.send', 'event', 'Gif generator', 'share', 'Facebook');
      FB.ui(
          {
              method: 'share',
              href: shareLink
          }
      );
  }

  function shareViaTwitter() {
    ga('send', 'event', 'Gif generator', 'share', 'Twitter');
    ga('1000heads.send', 'event', 'Gif generator', 'share', 'Twitter');
    window.open(twitterLink, '_blank');
  }

  function downloadImage() {
    ga('send', 'event', 'Gif generator', 'share', 'Download');
    ga('1000heads.send', 'event', 'Gif generator', 'share', 'Download');
    window.open(downloadLink);
  }

  function createTwitterLink() {
    let copy = encodeURIComponent("I'm #ReadyForRealLife with @SoftandGentleUK! Play the quiz for your chance to win a product bundle");
    let link = `https://twitter.com/intent/tweet?text=${copy}&url=${shareLink}`;
    return link;
  }

  function reloadPage() {
    location.reload();
  }

  function scrollToProducts(e) {
    e.preventDefault();
    ga('send', 'event', 'Gif generator', 'click', 'Find out more');
    ga('1000heads.send', 'event', 'Gif generator', 'click', 'Find out more');

    // scroll to products
    let element = document.getElementById("productRange")
    element.scrollIntoView(true);
  }

  return (
      <ReactCSSTransitionGroup
        className="content-block__copy result"
        component="div"
        transitionName="fade"
        transitionEnterTimeout={800}
        transitionLeaveTimeout={500}
        transitionAppear
        transitionAppearTimeout={500}
      >
        <div>
          <img src={gifImage} alt="Ready for Real Life" className="img--result" width="512" height="288" />
          <div className={"share clearfix " + (gifId ? '' : 'hide')} >
            <img src="./dist/img/gif_generator-result-share.png" alt="Share" className="img--share" />
            <a className="btn--share fa fa-facebook-official" onClick={shareViaFacebook} title="Share via Facebook"></a>
            <a className="btn--share fa fa-twitter" onClick={shareViaTwitter} target="_blank" title="Share via Twitter"></a>
            <a className="btn--share fa fa-arrow-down" onClick={downloadImage} target="_blank" title="Download"></a>
          </div>
          <button className="btn--share btn--replay fa fa-repeat" onClick={reloadPage} title="Replay"><p>Replay</p></button>
        </div>
        <div className="cta">
          Share your scene to Facebook or Twitter.<br/>
          Click the arrow to download as a GIF.
          <a className="btn" href="#" onClick={scrollToProducts}>Find out more about our products</a>
        </div>
      </ReactCSSTransitionGroup>
  );
}

Result.propTypes = {
  options: PropTypes.object.isRequired
};

export default Result;
