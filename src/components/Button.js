import React from 'react';

class Button extends React.Component {
    constructor(props) {
        super(props);

        this.onClickHandler = this.onClickHandler.bind(this);
    }
    onClickHandler(){
        let target = (this.props.target) ? `${this.props.target}` : 'Button';
        let value = (this.props.value) ? `${this.props.value}` : `${this.props.href}`;

        if (this.props.onClick)  this.props.onClick();

        ga('send', 'event', target, 'Click', value);
        ga('1000heads.send', 'event', target, 'Click', value);
    }
    render() {
        let classname = (this.props.className) ? `${this.props.className}` : '';

        return (
            <a className={`btn ${classname}`} target="_blank" href={this.props.href} onClick={this.onClickHandler} >{this.props.buttonText}</a>
        );
    }
}

export default Button;
