import React from 'react';
import ProductItem from './ProductItem';

class ProductRange extends React.Component {
    render() {
        const product = this.props.translations.ProductRange;
        const productOne = product.productOne;
        const productTwo = product.productTwo;

        return (
            <div id="productRange" className="content-block product-range">
                    <ProductItem buttonText="View range"
                    imgSrc={productOne.productSrc}
                     productName={productOne.productName}
                     productDesc={productOne.productDesc}
                     productUrl={productOne.productUrl}
                     color="pink" />
                    <ProductItem buttonText="View range"
                    imgSrc={productTwo.productSrc}
                    productName={productTwo.productName}
                    productDesc={productTwo.productDesc}
                    productUrl={productTwo.productUrl}
                    color="pink" />
            </div>
        );
    }
}

export default ProductRange;
