import React from 'react';
import ContentBlockTitle from './ContentBlock__title';
import ContentBlockParagraph from './ContentBlock__paragraph';

class Hero extends React.Component {
    render() {
        return (
        <div className="content-block hero">
            <h1 className="hero-title">Ready for <span>Real Life</span></h1>
            <h2 className="hero-subtitle">Ready to tackle your everyday challenges</h2>
        </div>
        );
    }
}

export default Hero;
