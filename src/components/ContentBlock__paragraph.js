import React from 'react';

class ContentBlockParagraph extends React.Component {
    render() {
        return (
                <p className="content-block__paragraph">{this.props.paragraph}</p>
        );
    }
}

export default ContentBlockParagraph;
