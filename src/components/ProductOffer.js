import React from 'react';
import ContentBlockTitle from './ContentBlock__title';
import ContentBlockParagraph from './ContentBlock__paragraph';
import Button from './Button';


class ProductOffer extends React.Component {
    constructor(props) {
        super(props);

        this.handleProductButtonClick = this.handleProductButtonClick.bind(this);
    }
    handleProductButtonClick() {
        fbq('track', 'Purchase', { value: 1.50, currency: 'GBP'});
    }

    render() {
        return (
            <div className="content-block product-offer">
                <div className="content-block--contain clearfix">
                        <ContentBlockTitle
                        title={this.props.translations.ProductOffer.title} subtitle={this.props.translations.ProductOffer.tandc}/>
                        <Button className="white" buttonText={this.props.translations.ProductOffer.buttonText} href={this.props.translations.ProductOffer.href} onClick={this.handleProductButtonClick} target ="Stockists" value={this.props.translations.ProductOffer.trackingStore} />
                </div>
            </div>
        );
    }
}

export default ProductOffer;
