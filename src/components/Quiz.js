import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Question from '../components/Question';
import QuestionCount from '../components/QuestionCount';
import AnswerOption from '../components/AnswerOption';

const Quiz = ({options}) => {
  function renderAnswerOptions(key) {
    return (
      <AnswerOption
        key={key.content}
        image={key.image}
        answerContent={key.content}
        answerType={key.type}
        colour={key.colour}
        questionId={options.questionId}
        answer={options.answer}
        onAnswerSelected={options.onAnswerSelected}
      />
    );
  }

  let progress = {
    counter: options.questionId,
    total: options.questionTotal
  };

  return (
    <ReactCSSTransitionGroup
      className="content-block__img question-block"
      component="div"
      transitionName="fade"
      transitionEnterTimeout={800}
      transitionLeaveTimeout={500}
      transitionAppear
      transitionAppearTimeout={500}
    >
      <img src="./dist/img/paper-corner.png" className="paper-corner" />
      <div key={options.questionId}>
        <QuestionCount progress={progress} />
        <Question content={options.question} />
        <ul className="answerOptions clearfix">
          {options.answerOptions.map(renderAnswerOptions)}
        </ul>
      </div>
    </ReactCSSTransitionGroup>
  );
}

Quiz.propTypes = {
  options: PropTypes.object.isRequired
};

export default Quiz;
