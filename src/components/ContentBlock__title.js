import React from 'react';
import PropTypes from 'prop-types';

class ContentBlockTitle extends React.Component {
    render() {
    let title = (this.props.title) ? `${this.props.title}` : '';
    let subtitle = (this.props.subtitle) ? <h3>{this.props.subtitle}</h3> : '';
    let color = (this.props.color) ? `${this.props.color}` : '';
        return (
            <div className="content-block__title">
                <h2 className={color}>{title}</h2>
                {subtitle}
            </div>
        );
    }
}

ContentBlockTitle.propTypes = {
  color: PropTypes.string
};

export default ContentBlockTitle;
