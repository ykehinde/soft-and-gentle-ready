import React from 'react';
import ContentBlockTitle from './ContentBlock__title';
import ContentBlockParagraph from './ContentBlock__paragraph';
import Button from './Button';


class ScienceAnimation extends React.Component {
    render() {
        return (
            <div className="content-block science-animation">
                <div className="content-block--contain clearfix">
                    <div className="content-block__img">
                        <div className="fluidVideo">
                        <img className="play_btn animated vidDisabled" src="/dist/img/play_btn-pink.png" onClick={(e) => this.hideButton(e)}/>
                            <iframe className="fluidVideo-item" width="560" height="315" src={"https://www.youtube.com/embed/Netsty_qoPI?rel=0&amp;controls=0&amp;showinfo=0"} frameBorder="0" allowFullScreen></iframe>
                        </div>
                    </div>
                    <div className="content-block__copy">
                            <ContentBlockTitle
                            title={this.props.translations.ScienceAnimation.title}
                            color="pink" />
                            <ContentBlockParagraph paragraph={this.props.translations.ScienceAnimation.paragraph} />
                            <Button buttonText={this.props.translations.ScienceAnimation.buttonText} href={this.props.translations.ScienceAnimation.href} />
                    </div>
                </div>
            </div>
        );
    }

    hideButton(e) {
        e.target.setAttribute('class', 'play_btn animated fadeOut');
        ga('send', 'event', 'Science Animation', 'Play', 'https://www.youtube.com/embed/Netsty_qoPI');
        ga('1000heads.send', 'event', 'Science Animation', 'Play', 'https://www.youtube.com/embed/Netsty_qoPI');
    }
}

export default ScienceAnimation;
