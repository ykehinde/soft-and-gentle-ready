import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PropTypes from 'prop-types';
import Button from './Button';

const BuyNow = (params) => {

  function gaTracking() {
    ga('send', 'event', 'Stockists', 'Click', params.translations.ProductOffer.trackingStore);
    ga('1000heads.send', 'event', 'Stockists', 'Click', params.translations.ProductOffer.trackingStore);
  }
  
  return (
    <ReactCSSTransitionGroup
      className="content-block__img question-block"
      component="div"
      transitionName="fade"
      transitionEnterTimeout={800}
      transitionLeaveTimeout={500}
      transitionAppear
      transitionAppearTimeout={500}
    >
      <div className="questionCount">
        <h3>BUY NOW</h3>
      </div>
      <div className="quiz-product">
      <a href={params.translations.ProductOffer.href} onClick={gaTracking} target="_blank">
        <img className="quiz--buynow-img" src={params.translations.Quiz.productSrc} alt={params.translations.ProductOffer.title} title={params.translations.ProductOffer.title} />
      </a>
      <p className="desc">{params.translations.ProductOffer.title}</p>
        <Button buttonText="BUY NOW" href={params.translations.ProductOffer.href} target ="Stockists" value={params.translations.ProductOffer.trackingStore}/>
      </div>
      <a onClick={params.handler} className="fa fa-step-forward continue-quiz">
        <p>{params.translations.Quiz.skipButton}</p>
      </a>
    </ReactCSSTransitionGroup>
  );
}

BuyNow.propTypes = {
  handler: PropTypes.func.isRequired,
  translations: PropTypes.object.isRequired
};

export default BuyNow;
