import React from 'react';
import Button from './Button';

class ProductItem extends React.Component {
    render() {
        return (
            <div className="product-item">
                <a href={this.props.productUrl} target="_blank"><img src={this.props.imgSrc} alt={this.props.productName} title={this.props.productName} /></a>
                <h3 className={`${this.props.color}`}><a href={this.props.productUrl}>{this.props.productName}</a></h3>
                <p className="product-description">{this.props.productDesc}</p>
                <p><Button buttonText={this.props.buttonText} href={this.props.productUrl} /></p>
            </div>
        );
    }
}

export default ProductItem;
