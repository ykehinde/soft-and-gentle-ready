import React from 'react';
import PropTypes from 'prop-types';

const QuestionCount = ({progress}) => {
  let words = [
    'One','Two','Three','Four','Five','Six', 'Seven'
  ];
  return (
    <div className="questionCount">
      <h3>{words[progress.counter-1]} / {words[progress.total-1]}</h3>
    </div>
  );
}

QuestionCount.propTypes = {
  progress: PropTypes.object.isRequired
};

export default QuestionCount;
