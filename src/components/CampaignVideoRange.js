import React from 'react';
import ContentBlockTitle from './ContentBlock__title';
import ContentBlockParagraph from './ContentBlock__paragraph';
import RangeItem from './RangeItem';

class CampaignVideoRange extends React.Component {
    render() {
        const video = this.props.translations.CampaignVideoRange.video;

        return (
            <div className="content-block range">
                    <ContentBlockTitle 
                    title={this.props.translations.CampaignVideoRange.title}
                    color='purple'
                    />
                    <img className="range--background--mobile" src="/dist/img/video_background--mobile.png" alt=""/>
                    <img className="range--background" src="/dist/img/video_background.png" alt=""/>
                    <ContentBlockParagraph paragraph={this.props.translations.CampaignVideoRange.paragraph} />
                    <RangeItem 
                    videoUrl={video.one.videoUrl}
                    videoHeadline={video.one.videoHeadline}
                    imgSrc={video.one.videoImgSrc} />
                    <RangeItem 
                    videoUrl={video.two.videoUrl}
                    videoHeadline={video.two.videoHeadline}
                    imgSrc={video.two.videoImgSrc} />
                    <RangeItem 
                    videoUrl={video.three.videoUrl}
                    videoHeadline={video.three.videoHeadline}
                    imgSrc={video.three.videoImgSrc} />
            </div>
        );
    }
}

export default CampaignVideoRange;
