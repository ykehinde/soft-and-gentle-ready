import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import FacebookLogin from 'react-facebook-login';
import Button from './Button';

const QuizFaceboook = ({options}) => {
    return (
        <ReactCSSTransitionGroup
            className="content-block__copy facebook"
            component="div"
            transitionName="fade"
            transitionEnterTimeout={800}
            transitionLeaveTimeout={500}
            transitionAppear
            transitionAppearTimeout={500}
            >
            <div>
                <h2>Login via Facebook</h2>
                <p>Log in with Facebook to add a personal touch with your profile picture.</p>
                <FacebookLogin
                    appId="177856769422419"
                    autoLoad={false}
                    fields="picture.type(large)"
                    callback={options.onFacebookResponse}
                    icon="fa-facebook"
                />
                <a className="btn" onClick={options.onSkipFacebook}>{options.translations.Quiz.skipButton}</a>
            </div>
        </ReactCSSTransitionGroup>
    );
}

QuizFaceboook.propTypes = {
  options: PropTypes.object.isRequired
};

export default QuizFaceboook;
