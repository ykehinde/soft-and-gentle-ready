import React from 'react';
import update from 'immutability-helper';
import axios from 'axios';
import questions from '../helpers/questions';
import defaults from '../helpers/defaults';
import parameters from '../../server/config/parameters';
import QuizDescription from './QuizDescription';
import Quiz from './Quiz';
import QuizFacebook from './QuizFacebook';
import Result from './Result';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class QuizContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            counter: 0,
            questionId: 1,
            question: '',
            answerOptions: [],
            answer: '',
            results: [],
            started: false,
            portrait: defaults.defaultImage,
            id: '',
            facebookComplete: false,
            facebookLoggedIn: false
        };

        this.handleStartQuiz = this.handleStartQuiz.bind(this);
        this.handleAnswerSelected = this.handleAnswerSelected.bind(this);
        this.handleResetQuiz = this.handleResetQuiz.bind(this);
        this.handleSkipFacebook = this.handleSkipFacebook.bind(this);
        this.handleFacebookResponse = this.handleFacebookResponse.bind(this);
    }

    componentWillMount() {
        this.setState({
            question: questions[0].question,
            answerOptions: questions[0].answers
        });
    }

    handleStartQuiz() {
        this.setState({
            started: true
        })

        fbq('trackCustom', 'Start Quiz');
        ga('send', 'event', 'Gif generator', 'play', 'Start Quiz');
        ga('1000heads.send', 'event', 'Gif generator', 'play', 'Start Quiz');
    }

    handleAnswerSelected(event) {
        this.setUserAnswer(event.currentTarget.value);
        ga('send', 'event', 'Gif generator', 'answer', event.currentTarget.value);
        ga('1000heads.send', 'event', 'Gif generator', 'answer', event.currentTarget.value);

        setTimeout(() => {
            if (this.state.questionId >= questions.length) {
                return;
            }

            this.setNextQuestion();
        }, 300);
    }

    handleResetQuiz() {
        ga('send', 'event', 'Gif generator', 'click', 'Restart Quiz');
        ga('1000heads.send', 'event', 'Gif generator', 'click', 'Restart Quiz');
        this.setState({
            counter: 0,
            questionId: 1,
            answer: '',
            results: [],
            id: '',
            question: questions[0].question,
            answerOptions: questions[0].answers,
            facebookComplete: false
        });
    }

    handleSkipFacebook() {
        ga('send', 'event', 'Gif generator', 'facebook', 'Skip');
        ga('1000heads.send', 'event', 'Gif generator', 'facebook', 'Skip');
         this.setState({
            facebookComplete: true
        }, () => { this.sendAjaxRequest( {answers: this.state.results, portrait: this.state.portrait} ) });
    }

    handleFacebookResponse(response) {
        ga('send', 'event', 'Gif generator', 'facebook', 'Connect');
        ga('1000heads.send', 'event', 'Gif generator', 'facebook', 'Connect');
        this.setState({
            facebookLoggedIn: true,
            portrait: response.picture.data.url
        }, () => { this.sendAjaxRequest( {answers: this.state.results, portrait: this.state.portrait} ) });
    }

    setUserAnswer(answer) {
        let index = this.state.results.length;
        const updatedResults = update(this.state.results, {
            $push: [answer]
        });

        this.setState({
            results: updatedResults,
            answer: answer
        });
    }

    setNextQuestion() {
        let counter =this.state.counter, questionId = this.state.questionId;
            counter = this.state.counter + 1;
            questionId = this.state.questionId + 1;
            
            this.setState({
                counter: counter,
                questionId: questionId,
                question: questions[counter].question,
                answerOptions: questions[counter].answers,
                answer: ''
            });
    }

    getResults() {
        return this.state.results.length;
    }

    sendAjaxRequest(choices) {
        let url = `${parameters.BASE_URL}/server/generator`;

        axios({
            method: 'post',
            url: url,
            data: choices,
            json: true
        })
            .then((response) => { this.setState({id: response.data }) })
            .catch(err => console.log(err));
    }

    renderStart() {
        return (
            <ReactCSSTransitionGroup
                className="content-block__img start clearfix"
                component="div"
                transitionName="fade"
                transitionEnterTimeout={800}
                transitionLeaveTimeout={500}
                transitionAppear
                transitionAppearTimeout={500}
                >
                <img src="./dist/img/gif_generator-start.png" alt="Generate your real life" onClick={this.handleStartQuiz} />
            </ReactCSSTransitionGroup>
        );
    }

    renderQuiz() {
        let params = {
            answer: this.state.answer,
            answerOptions: this.state.answerOptions,
            questionId: this.state.questionId,
            question: this.state.question,
            questionTotal: questions.length,
            onAnswerSelected: this.handleAnswerSelected
        };

        return (
            <Quiz options={params} />
        );
    }

    renderFacebook() {
        let params = {
            onFacebookResponse: this.handleFacebookResponse,
            onSkipFacebook: this.handleSkipFacebook,
            translations: this.props.translations
        }
        return (
            <QuizFacebook options={params} />
        );
    }

    renderResult() {
        let params = {
            quizResults: this.state.results,
            portrait: this.state.portrait,
            id: this.state.id,
            onResetQuiz: this.handleResetQuiz,
            translations: this.props.translations
        };
        return (
            <Result options={params} />
        );
    }

    render() {
        let results = this.state.results.length;
        let component = !this.state.started && this.renderStart()
            || results < questions.length && this.state.started && this.renderQuiz()
            || this.state.facebookComplete && this.renderResult()
            || this.state.facebookLoggedIn && this.renderResult()
            || this.renderFacebook();
        let onehundred = (!this.state.started) ? 'onehundred' : '';

        return (
            <div className="content-block quiz">
                <div className={`content-block--contain clearfix ${onehundred} `}>
                    <div className="content-block--separator copy">
                        <div className="content-block__copy">
                            <QuizDescription />
                        </div>
                        <div className="en-scroll en-scroll--green animated"><span>Find out why our deodorant <br/>will work for you</span></div>
                    </div>
                    <div className="content-block--separator img clearfix">
                        {component}
                        <img src="./dist/img/paper-background.png" className="paper" />
                    </div>
                </div>
            </div>
        );
    }
}

export default QuizContainer;
