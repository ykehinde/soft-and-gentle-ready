import React from 'react';
import Hero from './components/Hero';
import ScienceAnimation from './components/ScienceAnimation';
import QuizContainer from './components/QuizContainer';
import CampaignVideoRange from './components/CampaignVideoRange';
import ProductOffer from './components/ProductOffer';
import ProductRange from './components/ProductRange';
import translations from './components/config/translations';
import parameters from '../server/config/parameters';

class Root extends React.Component {
    render() {
        return (
            <div className="content">
                <Hero translations={translations} />
                <QuizContainer translations={translations} />
                <ScienceAnimation translations={translations} /> 
                <CampaignVideoRange translations={translations} />
                <ProductRange translations={translations} />
            </div>
        );
    }
}

export default Root;
