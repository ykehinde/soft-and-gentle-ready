let quizQuestions = [
    {
        question: "It's 7.30am on a Monday, what are you dealing with?",
        answers: [
            {
                type: "dog",
                content: "The dog chewing your shoes",
                colour: "quizPink"
            },
            {
                type: "cat",
                content: "The cat getting under your feet",
                colour: "quizTurq"
            },
            {
                type: "kids",
                content: "Kids not getting dressed",
                colour: "quizBlue"
            },
            {
                type: "late",
                content: "Being late for everything",
                colour: "quizMauve"
            }
        ]
    },
    {
        question: "You've bookmarked a page in the latest Style at Home magazine, what's the look?",
        answers: [
            {
                type: "room_modern",
                content: "Modern Scandi-Minimalism",
                colour: "quizPink"
            },
            {
                type: "room_shabby",
                content: "Rustic Shabby-Chic",
                colour: "quizTurq"
            },
            {
                type: "room_persian",
                content: "Luxury Persian-Plush",
                colour: "quizBlue"
            },
            {
                type: "room_justme",
                content: "My own personal mixture",
                colour: "quizMauve"
            }
        ]
    },
    {
        question: "Your Mother-in-law has dropped off your birthday present, what is it?",
        answers: [
            {
                type: "watercolour",
                content: "Her latest water colour painting",
                colour: "quizPink"
            },
            {
                type: "plant",
                content: "A prickly plant",
                colour: "quizTurq"
            },
            {
                type: "taxidermy",
                content: "Taxidermy",
                colour: "quizBlue"
            },
            {
                type: "ornament",
                content: "Gaudy Ornament",
                colour: "quizMauve",
            }
        ]
    },
    {
        question: "The week is over! What are you doing with your Friday night?",
        answers: [
            {
                type: "netflix",
                content: "Binge-watching Netflix",
                colour: "quizPink",
            },
            {
                type: "dinner",
                content: "Dinner with friends",
                colour: "quizTurq",
            },
            {
                type: "nightclub",
                content: "Queueing to get into a nightclub",
                colour: "quizBlue",
            },
            {
                type: "takeaway",
                content: "Cheeky takeaway",
                colour: "quizMauve",
            }
        ]
    },
    {
        question: "It's time to book a holiday, what do you want to do?",
        answers: [
            {
                type: "barbados",
                content: "Sandy shores of Barbados",
                colour: "quizPink"
            },
            {
                type: "himalayas",
                content: "Trek through the Himalayas",
                colour: "quizTurq"
            },
            {
                type: "parks",
                content: "Center Parcs with the family",
                colour: "quizBlue"
            },
            {
                type: "booze",
                content: "Booze Cruise with the girls",
                colour: "quizMauve"
            }
        ]
    },
    {
        question: "The grind starts tomorrow, what are you doing with the last day of the weekend?",
        answers: [
            {
                type: "wardrobe",
                content: "Build that flat-pack wardrobe",
                colour: "quizPink"
            },
            {
                type: "yoga",
                content: "Restore your chi with Yoga",
                colour: "quizTurq"
            },
            {
                type: "quiz",
                content: "This Quiz!",
                colour: "quizBlue"
            },
            {
                type: "kids",
                content: "Day out with the kids",
                colour: "quizMauve"
            }
        ]
    },
    {
        question: "You’ve just been hit with an urgent deadline, what do you reach for to get you through?",
        answers: [
            {
                type: "lavender",
                content: "Tea with two sugars",
                colour: "quizPink"
            },
            {
                type: "wildrose",
                content: "Vanilla latte, extra shot",
                colour: "quizTurq"
            },
            {
                type: "active",
                content: "Green tea to balance the zen",
                colour: "quizBlue"
            },
            {
                type: "care",
                content: "Coconut water power",
                colour: "quizMauve"
            }
        ]
    }
];

export default quizQuestions;
