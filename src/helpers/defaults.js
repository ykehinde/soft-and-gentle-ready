let defaultQuizAnswers = {
    answers: [
        'dog',
        'room_modern',
        'watercolour',
        'netflix',
        'barbados',
        'wardrobe',
        'lavender'
    ]
};
let defaultImage = 'images/portrait.png';

export default { defaultQuizAnswers, defaultImage };
