const express           = require('express');
const database          = require('../helpers/mongo');
const respond           = require('../helpers/respond');
const parameters        = require('../config/parameters');
const generator         = require('./generator');
const moment            = require('moment');
const AWS               = require('aws-sdk');
const fs                = require('fs');
const router            = express.Router();

AWS.config.update({
    secretAccessKey: parameters.AWS_SECRET_ACCESS_KEY,
    accessKeyId: parameters.AWS_ACCESS_KEY_ID,
    region: parameters.AWS_REGION
});

const s3 = new AWS.S3();
let ip = '';

const SUPPORTED_MIME_TYPES = [
    'video/mp4',
    'image/gif'
];

const validateData = req => {
    req.checkBody('gif', 'Gif can\'t be empty').notEmpty();
    req.checkBody('video', 'Video can\'t be empty').notEmpty();
    req.checkBody('gif_id', 'Gif id can\'t be empty').notEmpty();
    req.checkBody('gif_url', 'Gif url can\'t be empty').notEmpty();
    req.checkBody('video_url', 'Video url can\'t be empty').notEmpty();
};

const normalizeData = data => {
    return {
        gif: data.gif,
        video: data.video,
        gif_id: data.gif_id,
        gif_url: data.gif_url,
        video_url: data.video_url,
        ip: ip,
        createdAt: data.date
    };
};

const saveDataAndUploadFile = data => {
    let dataWithDate = Object.assign(
        {
            date: moment().format(),
            ip: ip
        },
        data
    );

    database.getDatabaseConnection().collection('gif').save(normalizeData(dataWithDate))
        .then(() => console.log((new Date).toISOString().replace(/z|t/gi,' '),'Gif saved!'))
        .catch(err => console.log(err))
    ;
};

const getEntries = (id,callback) => {
    database
        .getDatabaseConnection()
        .collection('gif')
        .findOne({'gif_id':id}, (err,doc) => {
            (err || !doc) ? callback(err, null) : callback(null, doc);
        });
    ;
};

const upload = (data, res) => {
    let id = data.gif_id;
    saveDataAndUploadFile(data);

    uploadToS3(data.gif, id, 'gif', () => {
        uploadToS3(data.video, id, 'mp4', () => {
            return res.send(id);
        });
    });
}

const uploadToS3 = (file, id, format, callback) => {
    fs.readFile(`${__dirname}/output/${file}`, (err, data) => {
        if (err) { throw err; }

        let base64data = new Buffer(data, 'binary');
        s3.putObject({
            Bucket: parameters.S3_BUCKET_NAME,
            Key: `${id}.${format}`,
            Body: base64data,
            ACL: 'public-read'
        },  (resp) => {
            console.log((new Date).toISOString().replace(/z|t/gi,' '),'Successfully uploaded', format);

            if (format === 'gif') {
                fs.unlink(`${__dirname}/output/${id}_large.gif`);
                fs.unlink(`${__dirname}/output/${id}.gif`);
            } else {
                fs.unlink(`${__dirname}/output/${id}.mp4`);
            }

            callback();
        });
    });
}

router
    .get('/share/twitter/player/:id', (req, res) => {
        getEntries(req.params.id, (err, results) => {
            if (err || !results) { res.render('error'); return; }

            res.render('player', {
                source: results.video_url
            });
        });
    })
    .get('/share/:id', (req, res, next) => {
        let referrer = req.get('Referrer');
        if (referrer && referrer.includes('facebook') || referrer && referrer.includes('t.co')) {
            res.redirect("https://ready.softandgentle.com");
        } else {
            getEntries(req.params.id, (err, results) => {
                if (err || !results) { res.render('error'); return; }

                res.render('card', {
                    baseUrl: parameters.BASE_URL,
                    facebookAppId: parameters.FACEBOOK_APP_ID,
                    gifId: results.gif_id,
                    shareableUrl: `${parameters.BASE_URL}/server/share/${results.gif_id}`,
                    gifPath: results.gif_url,
                    videoPath: results.video_url,
                });
            });
        }
    })
    .post('/generator', (req, res) => {
        ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        generator.generate(req.body, (data) => {
            upload(data, res);
        }).catch((reason) => {
            // return on error
            return reason;
        });
    })
;

module.exports = router;
