const GIFEncoder        = require('gifencoder');
const fs                = require('fs');
const chokidar          = require('chokidar');
const request           = require('request');
const Canvas            = require('canvas');
const Image             = require('canvas').Image;
const ffmpeg            = require('fluent-ffmpeg');
const controllers       = require('./index');
const parameters        = require('../config/parameters');
const collection        = require('../helpers/collection');

let sizes = [
    {h:1600, w:900},
    {h:512, w: 288}
];

const createEncoder = (height, width) => {
    return new GIFEncoder(height, width);
}

const addImages = (choices, portrait) => {
    return new Promise( (resolve,reject) => {
        let dir = __dirname;
        let frame = '';
        let images = [];
        setProfilePicture(portrait).then(
            (res) => {
                return choices.map((choice, index) => {
                    images.splice(collection[choice].index, 0, `${dir}/${collection[choice].image}`);

                    if (choice.includes('room')) {
                        frame = `${dir}/${collection[choice].frame}`;
                    }

                    if (index == choices.length-1) {
                        images.splice(3, 0, res.portrait);
                        images.splice(4, 0, frame);
                        frame = null;
                        resolve({images: images, filename: res.filename});
                    }
                });
            }, (err) => console.log(err));
    });
}

const setProfilePicture = (portrait) => {
    return new Promise( (resolve,reject) => {
        let filename = Date.now().toString();
        if (portrait.includes('http')) {
            request.get({ url: portrait, encoding: null }, (err, res, body) => {
                if (err) throw err;
                fs.writeFileSync(`${__dirname}/output/fb_${filename}.jpg`, new Buffer(body, 'binary'));
                resolve({portrait: `${__dirname}/output/fb_${filename}.jpg`, filename: filename});
            });
        } else {
            if (portrait.length = 0 || !portrait) {
                return reject('Could not load portrait');
            } else {
                return resolve({portrait: `${__dirname}/${portrait}`, filename: filename});
            }
        }
    });
}

const drawImage = (ctx, img, x, y, width, height) => {
    ctx.drawImage(img, x, y, width, height);
}

const drawFrame = (encoder, ctx, img, x, y, scale, scaleX, scaleY) => {
    if (scale) {
        ctx.scale(scaleX,scaleY);
    }
    drawImage(ctx,img,x,y,img.width,img.height);
    encoder.addFrame(ctx);
}

const timer = (start) => {
    if ( !start ) return process.hrtime();
    var end = process.hrtime(start);
    return Math.round((end[0]*1000) + (end[1]/1000000));
}

module.exports = {
    generate(choices, callback) {
        return new Promise( (resolve,reject) => {
            addImages(choices.answers, choices.portrait).then((res) => {
                let start = timer();
                let largeEncoder = createEncoder(sizes[0].h, sizes[0].w);
                let images = res.images;
                let filename = res.filename;

                if (!res.images || !res.filename) {
                    return reject('Failed to load images');
                }

                largeEncoder.createReadStream().pipe(fs.createWriteStream(`${__dirname}/output/${filename}_large.gif`));
                largeEncoder.start();
                largeEncoder.setRepeat(-1);   // 0 for repeat, -1 for no-repeat
                largeEncoder.setDelay(0);  // frame delay in ms
                largeEncoder.setQuality(10); // image quality. 10 is default.

                let canvas = new Canvas(sizes[0].h, sizes[0].w);
                let ctx = canvas.getContext('2d');

                return images.map((image, index) => {
                    let img = new Image();
                    img.onload = () => {
                        (image.includes('fb')) ?
                            drawImage(ctx,img,335,415,86,img.height * (86/img.width)) : drawImage(ctx,img,0,0,1600,900);
                    }
                    img.src = image;

                    if (index == images.length-1) {
                        largeEncoder.addFrame(ctx);
                        largeEncoder.finish();

                        canvas = null;
                        ctx = null;
                        largeEncoder = null;
                        img = null;

                        let gifWatcher = chokidar.watch(`${__dirname}/output/${filename}_large.gif`, {
                            persistent: true,
                            awaitWriteFinish: {
                                stabilityThreshold: 2000,
                                pollInterval: 100
                            },
                        });
                        gifWatcher
                            .on('change', () => {
                                let encoder = createEncoder(sizes[1].h, sizes[1].w);

                                encoder.createReadStream().pipe(fs.createWriteStream(`${__dirname}/output/${filename}.gif`));
                                encoder.start();
                                encoder.setRepeat(0);   // 0 for repeat, -1 for no-repeat
                                encoder.setDelay(0);  // frame delay in ms
                                encoder.setQuality(8); // image quality. 10 is default.

                                let canvas = new Canvas(sizes[1].h, sizes[1].w);
                                let ctx = canvas.getContext('2d');
                                ctx.scale(1.5,1.5);

                                let img = new Image();
                                img.onload = function() {
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -250, -380, false, 1, 1);
                                    drawFrame(encoder, ctx, img, -220, -360, true, .8, .8);
                                    drawFrame(encoder, ctx, img, -190, -340, true, .8, .8);
                                    drawFrame(encoder, ctx, img, -160, -310, true, .8, .8);
                                    drawFrame(encoder, ctx, img, -130, -270, true, .8, .8);
                                    drawFrame(encoder, ctx, img, -90, -230, true, .8, .8);
                                    drawFrame(encoder, ctx, img, -60, -140, true, .8, .8);
                                    drawFrame(encoder, ctx, img, 0, 0, true, .82, .82);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                    drawFrame(encoder, ctx, img, 0, 0, false, 1, 1);
                                }
                                img.src = `${__dirname}/output/${filename}_large.gif`;
                                encoder.finish();

                                ctx = null;
                                encoder = null;
                                canvas = null;

                                console.log((new Date).toISOString().replace(/z|t/gi,' '),'Gif built in ' + ((timer(start) % 60000) / 1000).toFixed(0) + 's');
                                let gifFile = `${__dirname}/output/${filename}.gif`;

                                fs.stat(`${__dirname}/output/fb_${filename}.jpg`, (err,stat) => {
                                    (err == null) ? fs.unlink(`${__dirname}/output/fb_${filename}.jpg`) : null;
                                });
                                gifWatcher.close();
                                gifWatcher = null;

                                let mp4Watcher = chokidar.watch(gifFile, {
                                    persistent: true,
                                    awaitWriteFinish: {
                                        stabilityThreshold: 3000,
                                        pollInterval: 200
                                    },
                                });
                                mp4Watcher
                                    .on('change', () => {
                                    ffmpeg(gifFile)
                                        .output(`${__dirname}/output/${filename}.mp4`)
                                        .on('end', () => {
                                            let response = {
                                                gif: `${filename}.gif`,
                                                video: `${filename}.mp4`,
                                                gif_id: filename,
                                                gif_url: `${parameters.S3_BUCKET_BASE_URL}/${parameters.S3_BUCKET_NAME}/${filename}.gif`,
                                                video_url: `${parameters.S3_BUCKET_BASE_URL}/${parameters.S3_BUCKET_NAME}/${filename}.mp4`
                                            };
                                            mp4Watcher.close();
                                            mp4Watcher = null;

                                            if (global.gc) {
                                                global.gc();
                                            } else {
                                                console.log('Garbage collection unavailable.  Pass --expose-gc '
                                                  + 'when launching node to enable forced garbage collection.');
                                            }

                                            return resolve(callback(response));
                                        })
                                        .run();
                                });
                        });
                    }
                });
            });
        });
    }
};
