const CONTENT_TYPE_KEY          = 'Content-type';
const CONTENT_DISPOSITION_KEY   = 'Content-disposition';
const FILE_NAME_TOKEN           = '[FILE_NAME]';

const STATUS = {
    SUCCESS: 200,
    BAD: 400,
    ERROR: 404,
};

const CONTENT_TYPE = {
    JSON: 'application/json'
};

module.exports = {
    setJSONContentType(response) {
        response.setHeader(CONTENT_TYPE_KEY, CONTENT_TYPE.JSON);
    },
    sendSuccessResponse(response, message) {
        return response.status(STATUS.SUCCESS).send(message);
    },
    sendBadResponse(response, message) {
        return response.status(STATUS.BAD).send(message);
    }
};
