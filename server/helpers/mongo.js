const parameters    = require('../config/parameters');
const MongoClient   = require('mongodb').MongoClient;

let databaseConnection;

module.exports = {
    connectToServer(callback) {
        MongoClient.connect(parameters.DATABASE_CONNECTION_URL, (err, database) => {
            databaseConnection = database;
            callback(err);
        });
    },
    getDatabaseConnection() {
        return databaseConnection;
    }
};
