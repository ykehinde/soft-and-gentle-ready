const express           = require('express');
const bodyParser        = require('body-parser');
const expressValidator  = require('express-validator');
const pug               = require('pug');
const throng            = require('throng');
const app               = express();
const database          = require('./helpers/mongo');
const Raven             = require('raven');
const parameters        = require('./config/parameters.js');

Raven
    .config(
        parameters.SENTRY_DSN,
        {
            release: '0e4fdef81448dcfa0e16ecc4433ff3997aa53572'
        }
    )
    .install()
;

const PORT = process.env.NODE_PORT || 3000;

const startServer = () => app.listen(PORT, () => console.log('Listening on port ' + PORT));

app
    .use(Raven.requestHandler())
    .use(Raven.errorHandler())
    .use(require('express-status-monitor')())
    .use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    })
    .use(bodyParser.json())
    .use(expressValidator())
    .use(require('./controllers'))
    .use(express.static(__dirname + '/public'));

app.set('views', __dirname + '/public/views');
app.set('view engine', 'pug');

database.connectToServer(err => {
    if (err) return console.log(err);

    throng({
        lifetime: Infinity
      }, startServer);
})
